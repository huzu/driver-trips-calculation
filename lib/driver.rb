require 'time'
require_relative 'trip'
require_relative 'constants'
require 'pry'


class Driver

  # this method will output the driver logs along with total distance and avg speed
  def report(driver_logs)
    drivers = filter_initialize_drivers(driver_logs)
    trips = filter_trips(driver_logs)

    # we will do the actual calculation of trips done by a driver and update the data accordingly.
    summarized_trips = summarize_trips(trips)

    # merge the drivers and summarized trips results as if there are no trips for any driver OR there is a trip but no Driver command present then we can either show 0 miles from the initialized driver object OR the trip data if available.
    sorted_trips = sort_trips(drivers.merge(summarized_trips).values)
    create_reportable_trips(sorted_trips)
  end

  def filter_initialize_drivers(driver_logs)
    all_drivers = driver_logs.select{ |line| line.start_with? COMMAND_DRIVER }

    all_drivers.inject({}) do |driver_details, driver|
      driver_tokens = driver.split(RECORD_SPLIT_PATTERN)
      driver_name = driver_tokens[DRIVER_NAME_INDEX]
      trip_data = default_trip_data
      trip_data.driver_name = driver_name

      driver_details.merge!({ driver_name => trip_data })
      driver_details
    end
  end

  # initialize all the trips here and reject the respective ones
  def filter_trips(driver_logs)
    all_trips = driver_logs.select{ |line| VALID_TRIP_LINE_REGEX =~ line }

    all_trips.inject([]) do |trip_details, trip|
      trip_tokens = trip.split(RECORD_SPLIT_PATTERN)
      distance = trip_tokens[TRIP_DISTANCE_INDEX].to_f
      start_time = Time.parse(trip_tokens[START_TIME_INDEX])
      end_time = Time.parse(trip_tokens[END_TIME_INDEX])
      driver_name = trip_tokens[DRIVER_NAME_INDEX]
      trip = Trip.new(start_time, end_time, distance, driver_name)

      trip_details << { driver_name => trip }
      trip_details
    end.reject { |trip|
      speed = trip[trip.keys[0]].calc_speed
      speed == nil || speed < MINIMUM_SPEED || speed > MAXIMUM_SPEED
    }
  end

  def summarize_trips(trips)
    trips.reduce({}) {|trip_summaries,trip|
      driver_name = trip.keys[0]

      # only those trips which are new to be added in final summaries, rest all which are already present in the trip_summaries needs to update the trip data after this if block
      if (!trip_summaries[driver_name])
        trip_data = default_trip_data
        trip_data.driver_name = driver_name
        trip_summaries[driver_name] = trip_data
      end

      update_summary_distance(trip_summaries, trip, driver_name)
      update_summary_minutes(trip_summaries, trip, driver_name)

      trip_summaries
    }
  end

  # we will add distance for the drivers for the respective trips created
  def update_summary_distance(trip_summaries, trip, driver_name)
    trip_summaries[driver_name].add_distance(trip[driver_name].distance)
  end

  # we will add up minutes for the drivers for the respective trips created
  def update_summary_minutes(trip_summaries, trip, driver_name)
    trip_summaries[driver_name].add_trip_minutes(trip[driver_name].trip_minutes)
  end

  # sort trips according to distance in decreasing order
  def sort_trips(trips)
    trips.sort_by { |trip| -trip.distance }
  end

  def create_reportable_trips(driver_trips)
    reported_trips = driver_trips.map do |value|
      speed = value.calc_speed
      if (speed)
        "#{value.driver_name}: #{value.distance.round.to_i} miles @ #{speed.round.to_i} mph"
      else
        "#{value.driver_name}: #{value.distance.round.to_i} miles"
      end
    end

    reported_trips
  end

  def default_trip_data
    default_time = Time.now
    Trip.new(default_time, default_time, 0, nil)
  end
end