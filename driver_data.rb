require_relative 'lib/driver'

input_data = []
while input_line = gets
  input_data << input_line.chomp
end

driver = Driver.new
report_lines = driver.report(input_data)

report_lines.each do |line|
  puts line
end