# README
## Problem Understanding and Walkthrough

Upon my initial review of the exercise description I felt that implementing classes for both of the entities `Driver` and `Trip` was the best starting point. This allows us to keep the logic and associated data organized.

Hence, I have created class for both the entities. There are certain operations like add minutes and distance to a trip of a particular driver which makes this particular scenario perfect to apply the Adapter design pattern. Though in the current implementation of the problem statement, we have applied ADP partially.

All the factory method for driver and trip like `update_summary_distance`, `update_summary_minutes`, `calc_speed` etc were added in the respective classes accordingly.
We have also added Rspecs for all the required operations.

Lastly, I have created `driver_data.rb` file which takes input as a file from the command line and prints the data respectively.


# Setup

1) Clone the repository
2) run `bundle install`
3) Using file input, enter `ruby driver_data.rb driver_logs.txt`
4) You can run specs using `rake` as the default task is to run Rspecs tests.